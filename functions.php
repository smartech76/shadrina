<?php
/**
 * Created by PhpStorm.
 * User: Пользователь
 * Date: 03.07.2018
 * Time: 16:24
 */
function enqueue_styles() {
    wp_register_style('app', get_template_directory_uri() . '/css/app.css');
    wp_register_style('bootstrap', get_template_directory_uri() . '/css/bootstrap.css', array(), 4.1);
    wp_register_style('bootstrap-grid', get_template_directory_uri() . '/css/bootstrap-grid.css', array(), 4.1);
    wp_register_style('bootstrap-reboot', get_template_directory_uri() . '/css/bootstrap-reboot.css', array(), 4.1);
    wp_register_style('slick', get_template_directory_uri() . '/css/slick.css');
    wp_register_style('slick-theme', get_template_directory_uri() . '/css/slick-theme.css');

    wp_enqueue_style( 'bootstrap');
    wp_enqueue_style( 'bootstrap-grid');
    wp_enqueue_style( 'bootstrap-reboot');
    wp_enqueue_style( 'slick');
    wp_enqueue_style( 'slick-theme');
    wp_enqueue_style( 'app');
}
add_action('wp_enqueue_scripts', 'enqueue_styles');

function enqueue_scripts () {
    wp_register_script('jquery', '//ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js');
    wp_register_script('slick-js', get_template_directory_uri() . '/js/slick.js', array('jquery'));
    wp_register_script('bootstrap-js', get_template_directory_uri() . '/js/bootstrap.js');
    wp_register_script('bootstrap-bundle', get_template_directory_uri() . '/js/bootstrap.bundle.js');
    wp_register_script('slick-ini', get_template_directory_uri() . '/js/slick-ini.js');
    wp_register_script('app-js', get_template_directory_uri() . '/js/app.js');
    wp_register_script('anchor', get_template_directory_uri() . '/js/anchor.js');


    wp_enqueue_script('jquery');
    wp_enqueue_script('slick-js');
    wp_enqueue_script('slick-ini');
    wp_enqueue_script('bootstrap-js');
    wp_enqueue_script('bootstrap-bundle');
    wp_enqueue_script('app-js');
    wp_enqueue_script('anchor');
}
add_action('wp_enqueue_scripts', 'enqueue_scripts');

if (function_exists('add_theme_support')) {
    add_theme_support('menus');
}