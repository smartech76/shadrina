jQuery(document).ready(function () {

    var $slide_one = jQuery('#slide_one');

    var $slide_two = jQuery('#slide_two');
    var $slide_three = jQuery('#slide_three');
    // var $slidermain = jQuery('#slidermain');

    jQuery($slide_one).addClass('title-block-visible');

    jQuery('#slidermain').bind('slid.bs.carousel', function (e) {

        var $index_slide = e.to;

        console.log(e);

        switch ($index_slide) {
            case 0:
                jQuery($slide_one).addClass('title-block-visible');
                jQuery($slide_three).removeClass('title-block-visible');
                break;
            case 1:
                jQuery($slide_two).addClass('title-block-visible');
                jQuery($slide_one).removeClass('title-block-visible');
                break;
            case 2:
                jQuery($slide_three).addClass('title-block-visible');
                jQuery($slide_two).removeClass('title-block-visible');
                break;
        }

    });

});
