<?php
/**
 * Created by PhpStorm.
 * User: Пользователь
 * Date: 03.07.2018
 * Time: 16:24
 */?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta http-equiv="Content-type" content="text/html; charset=<?php bloginfo('charset'); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <title><?php wp_title('«', true, 'right'); ?> <?php bloginfo('name'); ?></title>
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
    <?php wp_head(); ?>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
</head>
<body <?php body_class(); ?>>
<header>
    <div class="container-fluid header position-fixed d-flex justify-content-around text-white clearfix">
        <div class="row">
            <div class="col-sm">
                <img class="img-fluid" src="<?php echo get_template_directory_uri() . '/images/logo-pm-1.jpg'?>" alt="">
            </div>
            <div class="col-sm-3 d-flex align-items-center">
                <span>Готовые дома от застройщика в черте города</span>
            </div>
            <div class="col-sm-5 d-flex align-items-center">
                <ul class="nav">
                    <li class="nav-item">
                        <a class="nav-link active" href="#ex1">GHjtrns ljvjq</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#ex2">Ghtbveotcndf</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#ex3">Bgjntrf</a>
                    </li>
                </ul>
            </div>
            <div class="col-sm d-flex align-items-center">
                <span>8 (4852) 999 99 99</span>
            </div>
        </div>
    </div>
</header>

