<?php
/**
 * Created by PhpStorm.
 * User: Пользователь
 * Date: 03.07.2018
 * Time: 16:51
 */?>
<?php get_header(); ?>
<div class="container-fluid">
    <div class="row">
        <div id="slidermain" class="carousel slide" data-ride="false">
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img class="d-block w-100" src="<?php echo get_template_directory_uri() . '/images/slide-1.png?auto=no&bg=777&fg=555&text=First slide'?>"
                         alt="First slide">
                    <div id="slide_one" class="title-block carousel-caption carousel-fade d-none d-md-block">
                        <h5 class="text-danger">egeargerg</h5>
                        <p class="text-danger">sghsrthsrh</p>
                    </div>
                </div>
                <div class="carousel-item">
                    <img class="d-block w-100" src="<?php echo get_template_directory_uri() . '/images/slide-1.png?auto=no&bg=666&fg=444&text=Second slide'?>"
                         alt="Second slide">
                    <div id="slide_two" class="carousel-caption carousel-fade d-none d-md-block title-block">
                        <h5 class="text-danger">egeargerg</h5>
                        <p class="text-danger">sghsrthsrh</p>
                    </div>
                </div>
                <div class="carousel-item">
                    <img class="d-block w-100" src="<?php echo get_template_directory_uri() . '/images/slide-1.png?auto=no&bg=555&fg=333&text=Third slide'?>"
                         alt="Third slide">
                    <div id="slide_three" class="carousel-caption carousel-fade d-none d-md-block title-block">
                        <h5 class="text-danger">egeargerg</h5>
                        <p class="text-danger">sghsrthsrh</p>
                    </div>
                </div>
            </div>
            <a class="carousel-control-prev" href="#slidermain" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#slidermain" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>
</div>
<!--контейнер с фото котеджей-->
<div class="container-fluid back-black " id="ex1">
    <div class="container">
        <div class="row">
            <div class="col-sm">
                <div class="second-slider">
                    <div>
                        <img class="img-fluid" src="<?php echo get_template_directory_uri() . '/images/news-2.png'?>" alt="">
                    </div>
                    <div>
                        <img class="img-fluid" src="<?php echo get_template_directory_uri() . '/images/news-img-1.png'?>" alt="">
                    </div>
                    <div>
                        <img class="img-fluid" src="<?php echo get_template_directory_uri() . '/images/news-3.png'?>" alt="">
                    </div>
                    <div>
                        <img class="img-fluid" src="<?php echo get_template_directory_uri() . '/images/news-4.png'?>" alt="">
                    </div>
                    <div>
                        <img class="img-fluid" src="<?php echo get_template_directory_uri() . '/images/news-4.png'?>" alt="">
                    </div>
                    <div>
                        <img class="img-fluid" src="<?php echo get_template_directory_uri() . '/images/news-4.png'?>" alt="">
                    </div>
                    <div>
                        <img class="img-fluid" src="<?php echo get_template_directory_uri() . '/images/news-4.png'?>" alt="">
                    </div>
                    <div>
                        <img class="img-fluid" src="<?php echo get_template_directory_uri() . '/images/news-4.png'?>" alt="">
                    </div>
                    <div>
                        <img class="img-fluid" src="<?php echo get_template_directory_uri() . '/images/news-4.png'?>" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--контейнер с блоками развлечения-->
<div class="container-fluid" id="ex2">
    <div class="row">
        <div class="col-sm">
            <div class="card border-0 my-sm-4 mx-sm-3">
                <img class="card-img-top img-fluid" src="<?php echo get_template_directory_uri() . '/images/cadr-2.png'?>" alt="Card images cap">
                <div class="card-body">
                    <h5 class="card-title">Название карточки</h5>
                    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the
                        card's content.</p>
                </div>
            </div>
        </div>
        <div class="col-sm">
            <div class="card border-0 my-sm-4 mx-sm-3">
                <img class="card-img-top img-fluid" src="<?php echo get_template_directory_uri() . '/images/cadr-2.png'?>" alt="Card images cap">
                <div class="card-body">
                    <h5 class="card-title">Название карточки</h5>
                    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the
                        card's content.</p>
                </div>
            </div>
        </div>
        <div class="col-sm">
            <div class="card border-0 my-sm-4 mx-sm-3">
                <img class="card-img-top img-fluid" src="<?php echo get_template_directory_uri() . '/images/cadr-2.png'?>" alt="Card images cap">
                <div class="card-body">
                    <h5 class="card-title">Название карточки</h5>
                    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the
                        card's content.</p>
                </div>
            </div>
        </div>
        <div class="col-sm">
            <div class="card border-0 my-sm-4 mx-sm-3">
                <img class="card-img-top img-fluid" src="<?php echo get_template_directory_uri() . '/images/cadr-2.png'?>" alt="Card images cap">
                <div class="card-body">
                    <h5 class="card-title">Название карточки</h5>
                    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the
                        card's content.</p>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm">
            <div class="card border-0 my-sm-4 mx-sm-3">
                <img class="card-img-top img-fluid" src="<?php echo get_template_directory_uri() . '/images/cadr-2.png'?>" alt="Card images cap">
                <div class="card-body">
                    <h5 class="card-title">Название карточки</h5>
                    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the
                        card's content.</p>
                </div>
            </div>
        </div>
        <div class="col-sm">
            <div class="card border-0 my-sm-4 mx-sm-3">
                <img class="card-img-top img-fluid" src="<?php echo get_template_directory_uri() . '/images/cadr-2.png'?>" alt="Card images cap">
                <div class="card-body">
                    <h5 class="card-title">Название карточки</h5>
                    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the
                        card's content.</p>
                </div>
            </div>
        </div>
        <div class="col-sm">
            <div class="card border-0 my-sm-4 mx-sm-3">
                <img class="card-img-top img-fluid" src="<?php echo get_template_directory_uri() . '/images/cadr-2.pn'?>g" alt="Card images cap">
                <div class="card-body">
                    <h5 class="card-title">Название карточки</h5>
                    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the
                        card's content.</p>
                </div>
            </div>
        </div>
        <div class="col-sm">
            <div class="card border-0 my-sm-4 mx-sm-3">
                <img class="card-img-top img-fluid" src="<?php echo get_template_directory_uri() . '/images/cadr-2.png'?>" alt="Card images cap">
                <div class="card-body">
                    <h5 class="card-title">Название карточки</h5>
                    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the
                        card's content.</p>
                </div>
            </div>
        </div>
    </div>
</div>
<!--контейнер "запишитесь"-->
<div class="container-fluid responsive-container d-flex align-items-center justify-content-end " id="ex3">
    <div class="row">
        <div class="col-sm text-white">
            <span class="responsive-title">Pfujkjdjr</span>
            <p>
                ngsljgnsfnrsngsn.a.m
            </p>
            <div class="d-inline-flex">
                <button type="button" class="btn btn-success">Успех</button>
            </div>
        </div>
    </div>
    <div class="col-sm-7"></div>
</div>
<!--контейнер с отзывами-->
<div class="container-fluid" id="ex4">
    <div class="container">
        <div class="row">
            <div class="col-sm">
                <div class="card border-0 my-sm-4 mx-sm-3">
                    <img class="card-img-top img-fluid rounded-circle" src="<?php echo get_template_directory_uri() . '/images/turciy-img.png'?>" alt="Card images cap">
                    <div class="card-body">
                        <h5 class="card-title">Название карточки</h5>
                        <p class="card-text">Some quick example text to build on the card title and make up the bulk of
                            the card's content.</p>
                    </div>
                </div>
            </div>
            <div class="col-sm">
                <div class="card border-0 my-sm-4 mx-sm-3">
                    <img class="card-img-top img-fluid rounded-circle" src="<?php echo get_template_directory_uri() . '/images/turciy-img.png'?>" alt="Card images cap">
                    <div class="card-body">
                        <h5 class="card-title">Название карточки</h5>
                        <p class="card-text">Some quick example text to build on the card title and make up the bulk of
                            the card's content.</p>
                    </div>
                </div>
            </div>
            <div class="col-sm">
                <div class="card border-0 my-sm-4 mx-sm-3">
                    <img class="card-img-top img-fluid rounded-circle" src="<?php echo get_template_directory_uri() . '/images/turciy-img.png'?>" alt="Card images cap">
                    <div class="card-body">
                        <h5 class="card-title">Название карточки</h5>
                        <p class="card-text">Some quick example text to build on the card title and make up the bulk of
                            the card's content.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--контейнер с картой-->
<div class="container-fluid"></div>
<section>
    <?php if (have_posts()): while (have_posts()): the_post(); ?>
        <?php the_content(); ?>
    <?php endwhile; endif; ?>
</section>
<?php get_footer(); ?>
